'''
Created on 24.05.2018

@author: markus
'''
import socket

myIp = input("IP-Adresse: ")
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((myIp, 33333))
try:
    while True:
        nachricht = input("Nachricht: ")
        s.send(nachricht.encode())
        antwort = s.recv(2048)
        print("[{}] {}".format(myIp, antwort.decode()))
        if nachricht =='bye':
            break
finally:
    s.close()
    
